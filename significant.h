// MIT License
//
// Copyright (c) 2021 Oliver Ruiz Dorantes
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

static constexpr significant isSet(significant bit, significant value) {
    return (value >> bit) & 0x1u;
}

template <significant x>
struct BitsSet {
    static const significant n = BitsSet<(x>>1)>::n + (x&1);
};
template <>
struct BitsSet<0> {
    static const significant n = 0;
};


template <significant x>
struct SignificantBits {
    static const significant n = SignificantBits<(x >> 1)>::n + (1);
};
template <>
struct SignificantBits<0> {
    static const significant n = 0u;
};


template <significant width, significant x>
struct MostSignificant {
    static const significant lastbit = width - 1;
    static const significant mask = 1 << (lastbit);
    // Multiplication forces corner case
    static constexpr significant n = isSet(lastbit, x) + MostSignificant<width, (x << 1) * isSet(lastbit, x)>::n;
};
template <significant width>
struct MostSignificant<width, 0u> {
    static const significant n = 0u;
};


template <significant x>
struct LeastSignificant {
    // Multiplication forces corner case
    static constexpr significant n = isSet(0, x) + LeastSignificant<(x >> 1) * isSet(0, x)>::n;
};
template <>
struct LeastSignificant<0u> {
    static const significant n = 0u;
};
