#include <stdio.h>

typedef unsigned long int significant;
#include "significant.h"


#define VALUE 0xFFFFFFFFFFFFFFF1

struct SignificantBits<VALUE> sbits;
struct BitsSet<VALUE> bits;
struct MostSignificant<64, VALUE> most;
struct LeastSignificant<VALUE> least;

int main(void) {
   printf("Significant: %ld Bits Set: %ld Most: %ld Least: %ld\n", sbits.n, bits.n, most.n, least.n);

   return 0;
}
